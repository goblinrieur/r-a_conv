# r-a_conv

roman to arabic &amp; reverse number converter functions demo

# run example 

![image](./display.png)

## LICENSE

read [license](./LICENSE)

read [autodoc](./autodoc.doc)

# Docker container

`docker run -it localraconv:0.1`

if not build locally yet just 

`docker build -t localraconv:0.1 .`

Display will just be the same (bash & gforth). 

