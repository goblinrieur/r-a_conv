#! /usr/bin/env bash

runner=$(which gforth-fast)
capslock=$(which xdotool)

if [[ ${runner} != *"gforth-fast"* ]] ; then 
	 echo "Gforth interpreter not found"
	 exit 1
fi
trap '' SIGINT
$runner -W ./roman-arabic-converter.fs
clear 
trap SIGINT
tput cnorm
exit $?
