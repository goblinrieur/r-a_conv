FROM debian:11-slim

RUN apt update -y && \
	apt install -y gforth bash && \
	apt autoremove -y && \
    rm -rf /var/cache/apt

COPY *.fs ./
COPY *.sh ./

ENTRYPOINT ["bash","-c","./run.sh"]

